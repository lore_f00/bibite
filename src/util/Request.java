package util;

import java.util.HashMap;

public class Request {

    private HashMap<String, Object> inputData = new HashMap<>();

    public Request addInputData(String key, Object value) {
        this.inputData.put(key, value);
        return this;
    }

    public HashMap<String, Object> getInputData() {
        return this.inputData;
    }

}
