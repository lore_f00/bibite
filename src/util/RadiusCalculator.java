package util;

import entity.Point;

/**
 * Classe che calcola il raggio fra coordinate
 */
public class RadiusCalculator {

    public static final double EARTH_RADIUS = 6372.8; // raggio medio della terra

    /**
     * Calcola il raggio della circonferenza con base le coordinate dei
     *  due punti in input
     * @param point1
     * @param point2
     * @return radius
     */
    public static double calculateRadiusFromPoints(Point point1, Point point2) {
        return calculateRadius(point1.getLatitude(), point1.getLongitude(), point2.getLatitude(), point2.getLongitude());
    }

    /**
     * Calcola il raggio a partire dalle coordinate in input
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return radius
     */
    public static double calculateRadius(double lat1, double lng1, double lat2, double lng2) {
        return
                EARTH_RADIUS *
                Math.PI *
                Math.sqrt
                (
                        (lat2 - lat1) * (lat2 - lat1) +
                        Math.cos(lat2 / 57.29578) *
                        Math.cos(lat1 / 57.29578) *
                        (lng2 - lng1) *
                        (lng2 - lng1)
                ) / 180;
    }

}
