package repository;

import java.util.List;
import java.util.function.Predicate;

public interface RepositoryInterface<E> {

    /**
     * Metodo che si occupa di filtrare il contenuto della collezione
     *  tramite i criteri passati in input. Se criteria = null o è vuota
     * @param criteria criteri con cui filtrare gli oggetti nella collezione
     * @return elenco di oggetti presenti nella collezione che corrispondono ai criteri in input,
     *  eventualmente la lista può essere vuota se non sono presenti oggetti corrispondenti ai parametri
     */
    List<E> findByCriteria(Predicate<E> criteria);

    /**
     * Metodo che recupera il primo elemento della collezione che rispetta i criteri in input
     * @param criteria criteri con cui filtrare la collezione
     * @return il primo oggetto trovato corrispondente ai criteri, eventualmente null se non ci sono
     *  corrispondenze
     */
    E findOneByCriteria(Predicate<E> criteria);

    /**
     * Metodo che aggiunge un elemento alla collezione
     * @param e elemento da salvare, è richiesto che sia != null
     * @return true se va a buon fine
     */
    boolean save(E e);

}
