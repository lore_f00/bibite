package repository;

import entity.Customer;
import entity.Delivery;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DeliveryRepository extends AbstractRepository<Delivery> {

    public DeliveryRepository() {
        super(new ArrayList<>());
    }

    /**
     * Metodo che cerca le consegne prenotate per un certo cliente
     * @param customer cliente su cui basare la ricerca
     * @return l'elenco di consegne prenotate per quel cliente
     */
    public Delivery[] findRequestedDeliveriesByCustomer(Customer customer) {
        Objects.requireNonNull(customer);
        ArrayList<Delivery> deliveries;

        Predicate<Delivery> criteria = findByCustomer(customer);
        criteria = criteria.and(findByStatus(Delivery.Status.CREATED));

        deliveries = this.findByCriteria(criteria);
        return deliveries.toArray(new Delivery[0]);
    }

    /**
     * Metodo che cerca le prenotazioni per il cruscotto informativo
     * @return elenco con tutte le informazioni sulle consegne
     */
    public ArrayList<String> findForCruscotto() {
        ArrayList<Delivery> deliveries = findByCriteria(findByDate(LocalDate.now()));
        return (ArrayList<String>) deliveries
                .stream()
                .map(Delivery::getInformationPerCruscotto)
                .collect(Collectors.toList());
    }

    private Predicate<Delivery> findByDate(LocalDate date) {
        return delivery -> delivery.startInThisDate(date);
    }

    private Predicate<Delivery> findByCustomer(Customer customer) {
        return delivery -> delivery.hasThisCustomer(customer);
    }

    private Predicate<Delivery> findByStatus(Delivery.Status status) {
        return delivery -> delivery.getStatus().equals(status);
    }
}
