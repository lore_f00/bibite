package repository;

import entity.Customer;

import java.util.ArrayList;

public class CustomerRepository extends AbstractRepository<Customer> {

    public CustomerRepository() {
        super(new ArrayList<>());
    }

}
