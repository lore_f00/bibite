package repository;

import entity.Order;

import java.util.ArrayList;

public class OrderRepository extends AbstractRepository<Order> {

    public OrderRepository() {
        super(new ArrayList<>());
    }

}
