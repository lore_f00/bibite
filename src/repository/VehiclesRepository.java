package repository;

import entity.Vehicle;

import java.util.ArrayList;

public class VehiclesRepository extends AbstractRepository<Vehicle> {
    public VehiclesRepository() {
        super(new ArrayList<>());
    }
}
