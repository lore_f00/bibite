package repository;

import entity.Journey;

import java.util.ArrayList;

public class JourneyRepository extends AbstractRepository<Journey> {

    public JourneyRepository() {
        super(new ArrayList<>());
    }
}
