package repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Classe che implementa l'interfaccia RepositoryInterface usando come collezione una List
 *  il cui tipo reale può viene deciso nell'implementazione di questa classe
 * @param <E> tipo dei dati salvati nella collezione
 */
public class AbstractRepository<E> implements RepositoryInterface<E> {

     /**
     * Collezione degli elementi
     */
    protected List<E> collections;

    public AbstractRepository(List<E> collections) {
        this.collections = collections;
    }

    @Override
    public final ArrayList<E> findByCriteria(Predicate<E> criteria) {
        ArrayList<E> objectsToReturn = new ArrayList<>();
        this.collections.stream().filter(criteria).forEach(objectsToReturn::add);
        return objectsToReturn;
    }

    @Override
    public final E findOneByCriteria(Predicate<E> criteria) {
        ArrayList<E> objectsToReturn = findByCriteria(criteria);
        return objectsToReturn.size() > 0 ? objectsToReturn.get(0) : null;
    }

    @Override
    public final boolean save(E e) {
        Objects.requireNonNull(e);
        return this.collections.add(e);
    }

}
