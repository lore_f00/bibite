package factory;

import entity.*;
import util.Request;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class OrderFactory {

    /**
     * Metodo che sulla base della richiesta crea un ordine una tantum o un ordine periodico
     *  per l'utente che l'ha effettuata
     * @param request dati in input con cui creare l'ordine
     * @param customer utente che ha effettuato l'ordine
     * @return l'istanza dell'ordine creato
     */
    public Order create(Request request, Customer customer) {
        HashMap<String, Object> inputData = request.getInputData();
        if (!isValidInputData(inputData)) {
            throw new InvalidParameterException("Dati in input mancanti o non validi");
        }

        HashMap<Drink, Integer> drinksQuantities = (HashMap<Drink, Integer>) inputData.get("drinksQuantities");
        double price = this.calculatePrice(drinksQuantities);
        Order.OrderType orderType = (Order.OrderType) inputData.get("type");

        switch (orderType) {
            case UNA_TANTUM -> {
                return new OrderUnaTantum(
                        customer,
                        drinksQuantities,
                        price,
                        (LocalDate) inputData.get("requestedDeliveryDate")
                );
            }
            case PERIODIC -> {
                return new OrderPeriodic(
                        customer,
                        drinksQuantities,
                        price,
                        (int) inputData.get("periodInDays")
                );
            }
            default -> throw new InvalidParameterException();
        }
    }

    /**
     * Metodo che calcola il prezzo dell'ordine, senza nessun eventuale listino
     * @param drinksQuantities la quantità di bevande richieste dall'utente
     * @return il prezzo
     */
    private double calculatePrice(HashMap<Drink, Integer> drinksQuantities) {
        Objects.requireNonNull(drinksQuantities);
        double price = 0;
        // calcolo il prezzo dell'ordine, che non è quello effettivo che verrà calcolato con il listino
        for (Map.Entry<Drink, Integer> entry : drinksQuantities.entrySet()) {
            Drink drink = entry.getKey();
            price += drink.getPrice() * entry.getValue();
        }
        return price;
    }

    /**
     * Metodo che valida i dati in input, assicurandosi che siano tutti diversi da != null
     * @param inputData dati in input
     * @return true se sono tutti ok, false altrimenti
     */
    private boolean isValidInputData(HashMap<String, Object> inputData) {
        // calcolo il prezzo dell'ordine, che non è quello effettivo che verrà calcolato con il listino
        for (Map.Entry<String, Object> entry : inputData.entrySet()) {
            if (entry.getValue() == null) {
                return false;
            }
        }
        return true;
    }

}
