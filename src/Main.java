import entity.*;
import service.OrderManager;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        DrinkAgency agency = new DrinkAgency("Pissio inc", new Point(
                "Treviso",
                "TV",
                "piazza dello spritz 39",
                "31100",
                45.6679597,
                12.226776
        ));
        OrderManager orderManager = new OrderManager(agency);

        Customer customer = new Customer.CustomerBuilder("FCCLNZ00B05F770Q", "Lorenzo", "Facchin", new Point(
                "San Polo di Piave",
                "TV",
                "via pissio 10",
                "31020",
                45.7934276,
                12.3933404)
        ).withEmail("149248@spes.uniud.it").build();

        agency.saveCustomer(customer);

        Order order = orderManager.executeOrder(customer);
        System.out.println(order.getCustomer().getCf());

        Journey journey = new Journey(LocalDate.now());
        agency.saveJourney(journey);

        Delivery delivery = new Delivery(journey, order);
        agency.saveDelivery(delivery);
        journey.addDelivery(delivery);

        agency.calculateEffectivePrice(order, new PriceList("Inverno", "Inverno", 1.2));
        System.out.println(order.getPrice());
        System.out.println(order.getEffectivePrice());
        System.out.println(agency.getCruscottoInformativo());
    }

}
