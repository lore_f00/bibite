package entity;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Objects;

/**
 * ADT MUTABILE: rappresenta un'ordine singolo, che estende l'ordine semplice,
 *  con l'aggiunta della data di consegna richiesta
 */
public class OrderUnaTantum extends Order {

    protected final LocalDate requestedDeliveryDate;

    public OrderUnaTantum(Customer customer, HashMap<Drink, Integer> drinksQuantities, double price, LocalDate requestedDeliveryDate) {
        super(customer, drinksQuantities, price);
        this.requestedDeliveryDate = Objects.requireNonNull(requestedDeliveryDate);
        if (this.requestedDeliveryDate.toEpochDay() < LocalDate.now().toEpochDay()) {
            throw new InvalidParameterException("La data di ricezione dell'ordine deve essere >= oggi");
        }
        this.type = OrderType.UNA_TANTUM;
    }
}
