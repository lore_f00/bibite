package entity;

import repository.DeliveryRepository;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * ADT MUTABILE: rappresenta un viaggio, con l'elenco delle consegne da effettuare,
 *  un codice identificativo, il veicolo assegnato, la data in cui svolgerlo,
 *  lo stato i cui si trova e quando è effettivamente iniziato e terminato.
 *
 * INVARIANTI:
 * {
 *      status = CREATED => (startedAt == null && endedAt == null)
 *      status = IN_PROGRESS => (startedAt != null && endedAt == null)
 *      status = ENDED => (startedAt != null && endedAt != null)
 *      status != CREATED => vehicle != null,
 *      date > today
 * }
 *
 */
public class Journey {

    public enum Status {
        CREATED, IN_PROGRESS, ENDED
    }

    private final UUID uuid = UUID.randomUUID();
    private final DeliveryRepository deliveries = new DeliveryRepository();
    private Vehicle vehicle;
    private final LocalDate date;
    private Status status = Status.CREATED;
    private LocalDateTime startedAt;
    private LocalDateTime endedAt;

    public Journey(LocalDate date) {
        this.date = Objects.requireNonNull(date);
        if (this.date.toEpochDay() < LocalDate.now().toEpochDay()) {
            throw new InvalidParameterException("La data di partenza del viaggio deve essere >= oggi");
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public DeliveryRepository getDeliveries() {
        return deliveries;
    }

    public boolean addDelivery(Delivery delivery) {
        return deliveries.save(delivery);
    }


    public Vehicle getVehicle() {
        return vehicle;
    }

    public Journey setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

}
