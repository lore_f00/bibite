package entity;

import repository.*;

import java.util.ArrayList;
import java.util.Objects;

public class DrinkAgency {

    private final String name;
    private final Point address;
    private final OrderRepository orders = new OrderRepository();
    private final VehiclesRepository vehicles = new VehiclesRepository();
    private final JourneyRepository journeys = new JourneyRepository();
    private final DeliveryRepository deliveries = new DeliveryRepository();
    private final CustomerRepository customers = new CustomerRepository();
    private ArrayList<Drink> drinks = new ArrayList<>();

    public DrinkAgency(String name, Point address) {
        this.name = Objects.requireNonNull(name);
        this.address = Objects.requireNonNull(address);
        this.drinks.add(new Drink(
                "birra",
                "birra prova",
                Drink.DrinkType.BEER,
                101.1
        ));
        this.drinks.add(new Drink(
                "vino",
                "vino prova",
                Drink.DrinkType.WINE,
                11.1
        ));
    }

    /**
     * Metodo che salva un certo ordine nell'elenco degli ordini
     * @param order ordine da salvare
     */
    public void saveOrder(Order order) {
        orders.save(order);
    }

    /**
     * Metodo che salva un cliente nell'elenco dei clienti
     * @param customer cliente da censire
     */
    public void saveCustomer(Customer customer) {
        customers.save(customer);
    }

    /**
     * Metodo che salva una consegna nell'elenco delle consegne
     * @param delivery consegna da salvare
     */
    public void saveDelivery(Delivery delivery) {
        deliveries.save(delivery);
    }

    /**
     * Metodo che salva un viaggio nell'elenco dei viaggi
     * @param journey viaggio da salvare
     */
    public void saveJourney(Journey journey) {
        journeys.save(journey);
    }

    /**
     * Metodo che ricerca le consegne prenotate per un certo cliente
     * @param customer cliente per cui ricercare le prenotazioni
     * @return la lista delle prenotazione prenotate per quel cliente
     */
    public Delivery[] findRequestedDeliveriesByCustomer(Customer customer) {
        return deliveries.findRequestedDeliveriesByCustomer(customer);
    }

    /**
     * Metodo che ricerca le consegne in programma o completate in data odierna
     * @return lista di stringhe contententi le informazioni sulla consegna
     */
    public ArrayList<String> getCruscottoInformativo() {
        return deliveries.findForCruscotto();
    }

    /**
     * Metodo che permette di calcolare il prezzo effettivo per un'ordine sulla base di un listino Prezzi
     * @param order l'ordine di cui calcolare il prezzo
     * @param priceList il listino prezzi da usare
     * @return l'ordine con il prezzo effettivo
     */
    public Order calculateEffectivePrice(Order order, PriceList priceList) {
        return order.calculatePriceFromPriceList(priceList, this.address);
    }

    public String getName() {
        return name;
    }

    public Point getAddress() {
        return address;
    }

    public Drink[] getDrinks() {
        return drinks.toArray(new Drink[0]);
    }

}
