package entity;

import repository.JourneyRepository;

import java.time.LocalDate;
import java.util.Objects;

/**
 * ADT MUTABILE: rappresenta un veicolo, con la sua targa, la capacità di carico, l'anno di costruzione,
 *  l'ultima revisione e l'elenco dei viaggi svolti e da svolgere
 */
public class Vehicle {

    private final String plate;
    private final int capacity;
    private final int constructionYear;
    private LocalDate lastRevision;
    private final JourneyRepository journeys = new JourneyRepository();

    public Vehicle(String plate, int capacity, int constructionYear, LocalDate lastRevision) {
        this.plate = Objects.requireNonNull(plate);
        this.capacity = capacity;
        this.constructionYear = constructionYear;
        this.lastRevision = Objects.requireNonNull(lastRevision);
    }

    public String getPlate() {
        return plate;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getConstructionYear() {
        return constructionYear;
    }

    public LocalDate getLastRevision() {
        return lastRevision;
    }

    public Vehicle setLastRevision(LocalDate lastRevision) {
        this.lastRevision = lastRevision;
        return this;
    }

    public JourneyRepository getJourneys() {
        return journeys;
    }
}
