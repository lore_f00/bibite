package entity;

import java.util.Objects;

/**
 * ADT IMMUTABILE: rappresenta un punto nel mondo identificato dalla cittò,
 *  provincia, codice postale, latitudine e langitudine
 */
public class Point {

    private final String municipality;
    private final String province;
    private final String address;
    private final String zipCode;
    private final double longitude;
    private final double latitude;

    public Point(String municipality, String province, String address, String zipCode, double longitude, double latitude) {
        this.municipality = Objects.requireNonNull(municipality);
        this.province = Objects.requireNonNull(province);
        this.address = Objects.requireNonNull(address);
        this.zipCode = Objects.requireNonNull(zipCode);
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getMunicipality() {
        return municipality;
    }

    public String getProvince() {
        return province;
    }

    public String getAddress() {
        return address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
}
