package entity;

import repository.OrderRepository;

import java.util.Objects;

/**
 * ADT MUTABILE: rappresenta un cliente identificato dal codice fiscale, nome, cognome
 *  ed eventuale telefono o email o entrambe. Il cliente inoltre ha un indirizzo di consegna,
 *  e l'elenco degli ordini da lui effettuati.
 *
 * Esempio di stato concreto:
 * {
 *     "FCCLNZ00B05F770Q",
 *     "Lorenzo",
 *     "Facchin",
 *     "149248@spes.uniud.it",
 *     null,
 *     point,
 *     { order1, order2, ... orderN }
 * }
 *
 * Invariante: cf, address, name and surname cannot be null & !(phone == null && email == null)
 */
public class Customer {

    private final String cf;
    private final String name;
    private final String surname;
    private String phone;
    private String email;
    private Point address;
    private final OrderRepository orders = new OrderRepository();

    public Customer(CustomerBuilder customerBuilder) {
        this.cf = customerBuilder.getCf();
        this.name = customerBuilder.getName();
        this.surname = customerBuilder.getSurname();
        this.phone = customerBuilder.getPhone();
        this.email = customerBuilder.getEmail();
        this.address = customerBuilder.getAddress();
    }

    @Override
    public boolean equals(Object obj) {
        Customer customer = (Customer) obj;
        return customer.getCf().equals(this.cf);
    }

    public String getCf() {
        return cf;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhone() {
        return phone;
    }

    public Customer setPhone(String phone) {
        if (this.email == null && phone == null) {
            throw new NullPointerException("Telefono o email necessari");
        }
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customer setEmail(String email) {
        if (email == null && this.phone == null) {
            throw new NullPointerException("Telefono o email necessari");
        }
        this.email = email;
        return this;
    }

    public Point getAddress() {
        return address;
    }

    public Customer setAddress(Point address) {
        this.address = Objects.requireNonNull(address);
        return this;
    }

    public void executeOrder(Order order) {
        orders.save(order);
    }

    public static class CustomerBuilder {

        private String cf;
        private String name;
        private String surname;
        private String phone;
        private String email;
        private Point address;

        public CustomerBuilder(String cf, String name, String surname, Point address) {
            this.cf = Objects.requireNonNull(cf);
            this.name = Objects.requireNonNull(name);
            this.surname = Objects.requireNonNull(surname);
            this.address = Objects.requireNonNull(address);
        }

        public Customer build() {
            if (this.phone == null && this.email == null) {
                throw new NullPointerException("Telefono o email necessari");
            }
            return new Customer(this);
        }

        public CustomerBuilder setCf(String cf) {
            Objects.requireNonNull(cf);
            this.cf = cf;
            return this;
        }

        public CustomerBuilder setName(String name) {
            Objects.requireNonNull(name);
            this.name = name;
            return this;
        }

        public CustomerBuilder setSurname(String surname) {
            Objects.requireNonNull(surname);
            this.surname = surname;
            return this;
        }

        public CustomerBuilder setAddress(Point address) {
            this.address = address;
            return this;
        }

        public CustomerBuilder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public CustomerBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public String getCf() {
            return cf;
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public String getPhone() {
            return phone;
        }

        public String getEmail() {
            return email;
        }

        public Point getAddress() {
            return address;
        }
    }

}
