package entity;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.util.HashMap;

/**
 * ADT MUTABILE: rappresenta un'ordine periodico, che estende l'ordine semplice,
 *  con l'aggiunta del periodo in giorni, ovvero ogni quanto dovrà avvenire una consegna e quando è avvenuta l'ultima
 */
public class OrderPeriodic extends Order {

    private final int periodInDays;
    private LocalDate lastDelivery;

    public OrderPeriodic(Customer customer, HashMap<Drink, Integer> drinksQuantities, double price, int periodInDays) {
        super(customer, drinksQuantities, price);
        if (periodInDays <= 0) {
            throw new InvalidParameterException("Il periodo in giorni deve essere > 0");
        }
        this.periodInDays = periodInDays;
        this.type = OrderType.PERIODIC;
    }

    public int getPeriodInDays() {
        return periodInDays;
    }

    public LocalDate getLastDelivery() {
        return lastDelivery;
    }

    public OrderPeriodic setLastDelivery(LocalDate lastDelivery) {
        this.lastDelivery = lastDelivery;
        return this;
    }
}
