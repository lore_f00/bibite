package entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * ADT MUTABILE: rappresenta una consegna di un ordine per un certo cliente,
 *  con lo stato in cui si trova (in consegna, fallita, consegnata, creata),
 *  il viaggio di riferimento e la data di partenza e di consegna/tentata consegna
 *  e un codice identificativo
 *
 * Un esempio di stato concreto
 * {
 *     "unique_id",
 *     journey,
 *     order,
 *     CREATED,
 *     null,
 *     null
 * }
 *
 * INVARIANTE: startedAt != null => status != CREATED && endedAt => (status != CREATED && status != IN_PROGRESS)
 */
public class Delivery {

    public enum Status {
        CREATED, IN_PROGRESS, FAILED, DELIVERED
    }

    private final UUID uuid = UUID.randomUUID();
    private final Journey journey;
    private final Order order;
    private Status status = Status.CREATED;
    private LocalDateTime startedAt;
    private LocalDateTime endedAt;

    public Delivery(Journey journey, Order order) {
        this.journey = Objects.requireNonNull(journey);
        this.order = Objects.requireNonNull(order);
    }

    public LocalDate journeyDate() {
        return journey.getDate();
    }

    public boolean startInThisDate(LocalDate date) {
        return date.toEpochDay() == journey.getDate().toEpochDay();
    }

    /**
     * Metodo che verifica se la consegna fa riferimento all'ordine di un certo cliente
     * @param customer
     * @return true se la consegna è per il cliente in input, false altrimenti
     */
    public boolean hasThisCustomer(Customer customer) {
        Customer orderCustomer = this.order.getCustomer();
        return orderCustomer.equals(customer);
    }

    public String getInformationPerCruscotto() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("{\n")
                .append("uuid: ")
                .append(this.uuid)
                .append("\ncustomer: ")
                .append(this.getOrder().getCustomer().getCf());
        switch (this.getStatus()){
            case FAILED:
            case DELIVERED:
                getInformationEnded(stringBuilder);
            case IN_PROGRESS: getInformationInProgress(stringBuilder);
        }

        return stringBuilder.append("\n}").toString();
    }

    private void getInformationEnded(StringBuilder stringBuilder) {
        stringBuilder
                .append("\nstartedAt: ")
                .append(this.startedAt)
                .append("\nendedAt: ")
                .append(this.endedAt)
                .append("\nstatus: ")
                .append(this.status);
    }

    private void getInformationInProgress(StringBuilder stringBuilder) {
        stringBuilder
                .append("uuid: ")
                .append(this.uuid)
                .append("\nstartedAt: ")
                .append(this.startedAt);
    }

    public UUID getUuid() {
        return uuid;
    }

    public Journey getJourney() {
        return journey;
    }

    public Order getOrder() {
        return order;
    }

    public Status getStatus() {
        return status;
    }

    public Delivery setStatus(Status status) {
        this.status = status;
        return this;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public Delivery setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public LocalDateTime getEndedAt() {
        return endedAt;
    }

    public Delivery setEndedAt(LocalDateTime endedAt) {
        this.endedAt = endedAt;
        return this;
    }
}
