package entity;

import repository.DeliveryRepository;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

/**
 * ADT MUTABILE: rappresenta un'istanza dell'ordine, con un codice univoco, la tipologia,
 *  il cliente, il prezzo senza sconti e costo per km, il prezzo finale effettivo, il listino prezzi
 *  usato per calcolare il prezzo effettivo, l'elenco delle consegne effettuate e/o tentate, i prodotti scelti
 *  e la loro quantità.
 *
 * stato concreto
 * {
 *     "unique_id",
 *     CONFIRMED,
 *     UNA_TANTUM,
 *     customer,
 *     { (Drink1, 100), (Drink2, 200) },
 *     "2021-01-01"
 *     100,
 *     120,
 *     {}
 * }
 *
 * INVARIANTI:
 *  status = CONFIMED => effectivePrice >= 0 && deliveries.size() = 0;
 *  status = CREATED => effectivePrice = 0 && deliveries.size() = 0;
 */
public class Order {

    public enum OrderStatus {
        CREATED, CONFIRMED, DELETED, IN_PROGRESS, ENDED
    }

    /*
     * UNA_TANTUM = ordine singolo;
     * PERIODIC = ordine periodico;
     */
    public enum OrderType {
        UNA_TANTUM, PERIODIC
    }

    protected final UUID uuid = UUID.randomUUID();
    protected OrderStatus status = OrderStatus.CREATED;
    protected OrderType type;
    protected final Customer customer;
    protected final HashMap<Drink, Integer> drinksQuantities;
    protected final LocalDateTime createdAt = LocalDateTime.now();
    protected final double price;
    protected double effectivePrice;
    protected PriceList priceList;
    protected DeliveryRepository deliveries = new DeliveryRepository();

    public Order(Customer customer, HashMap<Drink, Integer> drinksQuantities, double price) {
        this.customer = Objects.requireNonNull(customer);
        this.drinksQuantities = Objects.requireNonNull(drinksQuantities);
        this.price = Objects.requireNonNull(price);
    }

    public UUID getUuid() {
        return uuid;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public Order setStatus(OrderStatus status) {
        this.status = status;
        return this;
    }

    public OrderType getType() {
        return type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public HashMap<Drink, Integer> getDrinksQuantities() {
        return drinksQuantities;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public double getPrice() {
        return price;
    }

    public double getEffectivePrice() {
        return effectivePrice;
    }

    /**
     * Calcola il prezzo effettivo dell'ordine sulla base del listino prezzi in input
     * @param priceList listino prezzi
     * @param agencyAddress indirizzo dell'agenzia, utile per calcolare il prezzo per km
     * @return this con effectivePrice = prezzo calcolato
     */
    public Order calculatePriceFromPriceList(PriceList priceList, Point agencyAddress) {
        double price = 0;
        price += Math.round(100.00 * priceList.calculatePricePerKm(agencyAddress, this.getCustomer().getAddress())) / 100.00;
        price += this.price;
        this.setPriceList(priceList);
        this.effectivePrice = price;
        return this;
    }

    public PriceList getPriceList() {
        return priceList;
    }

    public Order setPriceList(PriceList priceList) {
        this.priceList = priceList;
        return this;
    }
}
