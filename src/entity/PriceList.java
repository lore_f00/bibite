package entity;

import util.RadiusCalculator;

import java.util.Objects;

/**
 * ADT IMMUTABILE: rappresenta un listino con un nome, la descrizione
 *  e il costo per km. Possono essere presenti in futuro eventuali sconti
 *  da applicare all'ordine
 */
public class PriceList {

    private final String name;
    private final String description;
    private final double costPerKm;

    public PriceList(String name, String description, double costPerKm) {
        this.name = Objects.requireNonNull(name);
        this.description = Objects.requireNonNull(description);
        this.costPerKm = costPerKm;
    }

    /**
     * Metodo che calcola il prezzo in base alla distanza dalla sede aziendale
     * @param origin indirizzo di partenza
     * @param destination indirizzo di destinazione
     * @return il prezzo per la distanza
     */
    public double calculatePricePerKm(Point origin, Point destination) {
        return RadiusCalculator.calculateRadiusFromPoints(origin, destination) * this.costPerKm;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getCostPerKm() {
        return costPerKm;
    }
}
