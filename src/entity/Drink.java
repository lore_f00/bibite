package entity;

import java.util.Objects;

/**
 * ADT IMMUTABILE: rappresenta un prodotto dell'azienda di bibite,
 *  caratterizzato da un nome, una descrizione, una categoria e un prezzo
 *
 * Un esempio di stato concreto:
 * {
 *     "drink_name",
 *     "drink_description",
 *     WATER,
 *     1.05
 * }
 *
 * Invariante: price > 0 && name, description, type != NULL
 */
public class Drink {

    /*
     * Tipologie di bibita
     */
    public enum DrinkType {
        WATER, WINE, ENERGY_DRINK, BEER, SUPER_ALCOHOLIC
    }

    private final String name;
    private final String description;
    private final DrinkType type;
    private final double price;

    public Drink(String name, String description, DrinkType type, double price) {
        this.name = Objects.requireNonNull(name);
        this.description = Objects.requireNonNull(description);
        this.type = Objects.requireNonNull(type);
        this.price = Objects.requireNonNull(price);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public DrinkType getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }
}
