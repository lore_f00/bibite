package service;

import entity.Customer;
import entity.Drink;
import entity.DrinkAgency;
import entity.Order;
import factory.OrderFactory;
import util.Request;

import java.time.LocalDate;
import java.util.HashMap;

public class OrderManager {

    public OrderFactory orderFactory = new OrderFactory();
    private DrinkAgency drinkAgency;

    public OrderManager(DrinkAgency drinkAgency) {
        this.drinkAgency = drinkAgency;
    }

    /**
     * Metodo che permette al cliente di eseguire un ordine, leggendo i dati per poi crearlo,
     *  andando ad aggiungerlo all'elenco ordini dell'agenzia e del cliente
     * @param customer cliente che esegue l'ordine
     * @return l'istanza dell'ordine creato dall'utente, null in caso di errori
     */
    public Order executeOrder(Customer customer) {
        Request request = new Request();
        Order order = null;

        HashMap<Drink, Integer> drinksQuantities = new HashMap<>();
        drinksQuantities.put(getDrinks()[0], 10);
        drinksQuantities.put(getDrinks()[1], 5);
        request
                .addInputData("requestedDeliveryDate", LocalDate.now().plusDays(0))
                .addInputData("type", Order.OrderType.UNA_TANTUM)
                .addInputData("drinksQuantities", drinksQuantities);

        try {

            // TODO creazione ordine
            // faccio scegliere all'utente tutto il necessario per l'ordine e poi lo eseguo

            order = orderFactory.create(request, customer);
            customer.executeOrder(order);
            drinkAgency.saveOrder(order);

        } catch (Exception e) {

        }
        return order;
    }

    private Drink[] getDrinks() {
        return this.drinkAgency.getDrinks();
    }

}
